//
//  FirstViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/08/28.
//

import UIKit
import Network

class FirstViewController: UIViewController {
    
    let monitor = NWPathMonitor()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let infoLabel = Info(text: "FirstViewです！")
        infoLabel.ask()
        
        Cat.meow()

        // UILabelの設定
        let titleLabel = UILabel() // ラベルの生成
        titleLabel.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width, height: 44) // 位置とサイズの指定
        titleLabel.textAlignment = NSTextAlignment.center // 横揃えの設定
        titleLabel.text = "アプリのタイトル" // テキストの設定
        titleLabel.textColor = UIColor.white // テキストカラーの設定
        titleLabel.font = UIFont(name: "HiraKakuProN-W6", size: 17) // フォントの設定
        self.view.addSubview(titleLabel) // ラベルの追加
        checkWifiConnection()
    }
    

    func checkWifiConnection() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("通信環境あります！")
            } else {
                print("通信出来ません！")
            }
            
            print(path.isExpensive)
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }

}
