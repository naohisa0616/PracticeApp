//
//  Info.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/10/02.
//

import Foundation


class Info {
    
    var text: String
    
    init(text: String) {
        self.text = text
    }
    
    func ask() {
        print(text)
    }
    
}
