//
//  Cat.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/10/02.
//

import Foundation

class Cat {
    
    static let sex: String = "male"
    
    func eat() {}
    static func meow() {}
    
}
// メソッド呼び出し時にCatクラスのインスタンスが毎回生成される。Cat().eat()

// Catクラスのインスタンスを生成せずに参照する。Cat.meow()

// staticを付けなかった場合
// インスタンスメンバー「meow」はタイプ「Cat」では使用できません。
