//
//  CountUp.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/10/13.
//

import UIKit

class CountUp {
    static func countUp(num: Int) -> String {
        if num % 3 == 0 && num % 5 == 0 {
            return "fizz Buzz"
        } else if num % 3 == 0 {
            return "fizz"
        } else if num % 5 == 0 {
            return "Buzz"
        } else {
            return String(num)
        }
    }
}
