//
//  PageViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/08/28.
//

import UIKit

class PageViewController: UIPageViewController {
    
    //ページングしたいviewControllerをいれる
    private var controllers: [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initPageViewController()

    }
    
    func viewWillAppear() {
        self.initPageViewController()
    }
    
    // PageViewControllerの初期化処理
    private func initPageViewController() {

        // ② PageViewControllerで表示するViewControllerをインスタンス化する
        let firstVC = storyboard!.instantiateViewController(withIdentifier: "FirstView") as! FirstViewController
        let secondVC = storyboard!.instantiateViewController(withIdentifier: "SecondView") as! SecondViewController
        let ThirdVC = storyboard!.instantiateViewController(withIdentifier: "ThirdView") as! ThirdViewController

        // ③ インスタンス化したViewControllerを配列に保存する
        self.controllers = [ firstVC, secondVC, ThirdVC ]
        
        // ④ 最初に表示するViewControllerを指定する
        setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
       
        // ⑤ PageViewControllerのDataSourceを関連付ける
        self.dataSource = self
    }

}

// MARK: - UIPageViewController DataSource
extension PageViewController: UIPageViewControllerDataSource {
    
    //左にスワイプしたとき(ページが進む)
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//        index += 1
//        print(viewController)
//
//        if index >= controllers.count {
//            index = controllers.count
//            print(index)
//            return controllers[controllers.count - 1]
//        }
//        print(index)
//        print(controllers.count)
//
//        if 0 <= index && index < controllers.count {
//            return controllers[index]
//        }
//        return nil
        
        if viewController is FirstViewController {
            return controllers[1]
        } else if viewController is SecondViewController {
            return controllers[2]
        } else {
            return controllers[0]
        }
//        var index:Int = self.controllers.firstIndex(of: viewController)!
//        if index == NSNotFound {
//            return nil
//        }
//        index += 1
//        if 0 <= index && index < controllers.count {
//            return controllers[index]
//        }
//        return nil
    }
    
    //右にスワイプしたとき(ページが戻る)
    func pageViewController(_ pageViewController:UIPageViewController,viewControllerBefore viewController:UIViewController) -> UIViewController?{
        var index:Int = self.indexOfController(viewController:viewController)
        if index == NSNotFound {
            return nil
        }
        index -= 1
        if 0 <= index && index < controllers.count {
            return controllers[index]
        }
        return nil
    }
    
    //viewControllerのindexを返す
    func indexOfController(viewController:UIViewController) -> Int{
        for i in 0 ..< controllers.count{
            if(viewController == controllers[i]){
                return i
            }
        }
        return NSNotFound
    }
    
}
