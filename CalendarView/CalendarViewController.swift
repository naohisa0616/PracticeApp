//
//  CalendarViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/10/05.
//

import UIKit

class CalendarViewController: UIViewController {
    
    let mainView: UIView = UIView()
    var stackV: UIStackView = UIStackView()
    var buttonArray: [UIButton] = []
    var stkArray: [UIStackView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - メインビューの生成
        //mainViewの背景色
        mainView.backgroundColor = UIColor.gray
        //Autosizing（自動サイズ設定）をAutoLayoutに変換しないようにしている
        mainView.translatesAutoresizingMaskIntoConstraints = false
        //ビューを表示させる
        view.addSubview(mainView)
        // 横方向の中心は親ビューの横方向の中心と同じ
        mainView.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        // 縦方向の中心は親ビューの縦方向の中心と同じ
        mainView.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        // mainViewの幅は親ビューの90％
        mainView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.9).isActive = true
        // mainViewの高さはmainViewの横幅と同じ。（正方形になる）
        mainView.heightAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 1.0).isActive = true
        
        // MARK: - stackViewを生成（縦）
        //スタックビューを縦に設定
        stackV.axis = .vertical
        //中のオブジェクトをどこに揃えて配置するか決めている。
        stackV.alignment = .fill
        //縦にバランスよく配置
        stackV.distribution = .fillEqually
        //オブジェクト同士のスペース
        stackV.spacing = 4
        //Autosizingのレイアウトの仕組みをAuto Layoutに変換するかどうかを設定するフラグ
        stackV.translatesAutoresizingMaskIntoConstraints = false
        //stackVの背景色
        stackV.backgroundColor = UIColor.white
        //stackVを表示
        view.addSubview(stackV)
        
        //stackVの横方向の中心はmainViewの横方向の中心と同じ
        stackV.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        //stackVの縦方向の中心はmainViewの縦方向の中心と同じ
        stackV.centerYAnchor.constraint(equalTo: mainView.centerYAnchor).isActive = true
        //stackVの横幅はmainViewの90%
        stackV.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 0.9).isActive = true
        //stackVの高さはstackVの横幅と同じ。（正方形になる）
        stackV.heightAnchor.constraint(equalTo: stackV.widthAnchor, multiplier: 1).isActive = true
        
        // MARK: - stackViewを生成（横）
        //for文でStackView6つ生成
        for i in 0 ..< 6 {
            //StackViewの生成
            let stackH:UIStackView = UIStackView()
            //スタックビューを横に設定
            stackH.axis = .horizontal
            //オブジェクト同士のスペース
            stackH.spacing = 4
            //中のオブジェクトをどこに揃えて配置するか決めている。
            //alignmentで配置を決める
            //fillで塗りつぶし
            stackH.alignment = .fill
            //横にバランスよく配置
            stackH.distribution = .fillEqually
            
            //背景色を設定
            switch i {
            case 0:
                stackH.backgroundColor = UIColor.systemPink
            case 1:
                stackH.backgroundColor = UIColor.systemPurple
            case 2:
                stackH.backgroundColor = UIColor.systemBlue
            case 3:
                stackH.backgroundColor = UIColor.systemYellow
            case 4:
                stackH.backgroundColor = UIColor.systemOrange
            case 5:
                stackH.backgroundColor = UIColor.systemGreen
                
            default:
                return
            }
            //スタックビュー配列に追加
            stkArray.append(stackH)
        }
        //stackVの中にstackHを格納
        for i in 0 ..< 6 {
            stackV.addArrangedSubview(stkArray[i])
        }
        
        // MARK: - ボタン生成&配置
        //ボタンを42個生成
        var tagNumber = 1
        for _ in 0...41 {
            let button: UIButton = UIButton(type: .custom)
            button.setTitleColor(UIColor.black, for: UIControl.State())
            button.setTitle(String(tagNumber), for: UIControl.State())
            button.tag = tagNumber
            tagNumber += 1
            buttonArray.append(button)
        }
        
        //生成したボタンをStackViewに格納
        for i in 0 ..< 42 {
            switch i {
            case 0 ..< 7:
                stkArray[0].addArrangedSubview(buttonArray[i] as UIView)
            case 7 ..< 14:
                stkArray[1].addArrangedSubview(buttonArray[i] as UIView)
            case 14 ..< 21:
                stkArray[2].addArrangedSubview(buttonArray[i] as UIView)
            case 21 ..< 28 :
                stkArray[3].addArrangedSubview(buttonArray[i] as UIView)
            case 28 ..< 35:
                stkArray[4].addArrangedSubview(buttonArray[i] as UIView)
            case 35 ..< 42:
                stkArray[5].addArrangedSubview(buttonArray[i] as UIView)
            default:
                return
            }
        }
    }
}
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
