//
//  Person.h
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/09/12.
//

#ifndef Person_h
#define Person_h


#endif /* Person_h */

//@interface Person : NSObject
//{
//    // 名前
//    NSString *_name;
//
//    // 年齢
//    NSInteger _age;
//}
//// 名前を取得する
////インスタンス変数から値を取り出して返すメソッド（ゲッター）
//- (NSString *)name;
//// 名前を設定する
////インスタンス変数に値を代入するメソッド（セッター）
//- (void)setName:(NSString *)name;
//
//// 年齢を取得する
//- (NSInteger)age;
//// 年齢を設定する
//- (void)setAge:(NSInteger)age;

// 名前
//NSStringに＊がついている
//*とはNSStringとはNSString型へのポインタ(int型の値が格納されているメモリの先頭アドレス)という型を表す。
//@property (nonatomic) NSString *name;
//// 年齢
//@property (nonatomic) NSInteger age;
//
//- (BOOL)writeToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile encoding:(NSStringEncoding)enc error:(NSError **)error;

//
//@end
