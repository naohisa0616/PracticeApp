//
//  Person.m
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/09/12.
//

#import <Foundation/Foundation.h>
#import "Person.h"

//@implementation Person
//// 名前を取得する
////インスタンス変数から値を取り出して返すメソッド（ゲッター）
//- (NSString *)name
//{
//    return _name;
//}
//// 名前を設定する
////インスタンス変数に値を代入するメソッド（セッター）
//- (void)setName:(NSString *)name
//{
//    _name = name;
//}
//// 年齢を取得する
//- (NSInteger)age
//{
//    return _age;
//}
//// 年齢を設定する
//- (void)setAge:(NSInteger)age
//{
//    _age = age;
//}

//アクセサーを自分で定義
//- (NSString *)name
//{
//    return [NSString stringWithFormat:@"%@ 様", _name];
//}
//
//NSString *str = @"文字列";
//NSError *error = nil;
//[str writeToFile:@"write/to/path"
//      atomically:YES
//        encoding:NSUTF8StringEncoding
//           error:&error];

//@end
