//
//  TagBtnViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/08/27.
//

import UIKit
import Network
import AVFoundation

class TagBtnViewController: UIViewController {

    var pageViewController: UIPageViewController!
    var menuViewControllers: Array<UIViewController> = []
    var selected: Int!
    var vc1 = UIView()
    let button = UIButton()
    var posY: CGFloat!
    let crossBtnImage = UIImage(systemName: "play.circle")
    let whiteBtn = UIImage(systemName: "number.circle")
    let blackBtn = UIImage(systemName: "moon")
    let sunBtn = UIImage(systemName: "sun.min")
    var btnArray: [UIButton] = []
    let btnImageArray = ["play.circle","number.circle","moon","sun.min"]
    let monitor = NWPathMonitor()
    
    @IBAction func alertButton(_ sender: Any) {
        let alert = AlertController(title: "タイトル", message: "メッセージ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = .black



        let image = UIImage(systemName: "mic")
//        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 64, height: 64))
//        imageView.image = image
//        alert.view.addSubview(imageView)
//        let alert = AlertController()
        alert.setTitleImage(image)
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func audioButton(_ sender: Any) {

        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)

        if status == AVAuthorizationStatus.authorized {
            // アクセス許可あり
            print("authorized")
        } else if status == AVAuthorizationStatus.restricted {
            // ユーザー自身にアクセスが許可されていない
            print("restricted")
        } else if status == AVAuthorizationStatus.notDetermined {
            // まだアクセス許可を聞いていない
            print("notDetermined")
        } else if status == AVAuthorizationStatus.denied {
            // アクセス許可されていない
            let alert = UIAlertController(title: "マイクを許可する必要があります", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBOutlet weak var crossBtn: UIButton!
    
    @IBOutlet weak var tapBarScroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        crossBtn.setTitle("アイコン", for: .normal)
        
        tapBarScroll.delegate = self
        
        //ScrollView上に要素を配置
        horizontalScroll()
        
        for vc in children {
            if vc is PageViewController {
                pageViewController = vc as? PageViewController
            }
        }
        
        // PageViewにセットするViewControllerを生成して配列に入れる
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "FirstView") as! FirstViewController
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "SecondView") as! SecondViewController
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "ThirdView") as! ThirdViewController
        menuViewControllers.append(firstViewController)
        menuViewControllers.append(secondViewController)
        menuViewControllers.append(thirdViewController)

        // 初期表示をメニュー(FirstViewController)にする
        pageViewController.setViewControllers([menuViewControllers[0]], direction: .forward, animated: false, completion: nil)
        selected = 1
        
        checkWifiConnection()
        
    }
    
    func checkWifiConnection() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("通信環境あります！")
            } else {
                print("通信出来ません！")
            }
            
            print(path.isExpensive)
        }
    }
    
    //横スクロール
    func horizontalScroll() {
        //vcのframe
        vc1.frame = CGRect(x: 0, y: 0, width: tapBarScroll.frame.size.width * 2, height: tapBarScroll.frame.size.height)
        //上部のスクロールビューに多数のボタンを配置
        for i in 0...2 {
            let button = UIButton()
            //サイズ
            button.frame = CGRect(x: (i*100), y: 30, width: 80, height: 55)
            //タグ
            button.tag = i
            //buttonに文字を挿入
            setTitleForButton(tag: button.tag, button: button)
            //button.titleの色
            button.setTitleColor(.white, for: .normal)
            button.layer.borderWidth = 1
            // UIButtonの配列
            btnArray += [button]
            print(btnArray)
            //vcに載せる
            vc1.addSubview(button)
        }
        print(btnArray)
        //スクロールビューにvcを配置
        tapBarScroll.addSubview(vc1)
        //scrollViewのコンテンツサイズをvc1のサイズにする
        tapBarScroll.contentSize = vc1.bounds.size
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.button.setImage(whiteBtn, for: .normal)
    }
    
    //スクロールビューのボタンに文字を入れる
    func setTitleForButton(tag:Int, button:UIButton){
        switch tag {
        case 0:
            button.setTitle("FirstView", for: .normal)
            button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            button.setImage(whiteBtn, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
            button.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
        case 1:
            button.setTitle("SecondView", for: .normal)
            button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            button.setImage(sunBtn, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
            button.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
        case 2:
            button.setTitle("ThirdView", for: .normal)
            button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            button.setImage(crossBtnImage, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
            button.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
//        case 3:
//            button.setTitle("文学", for: .normal)
//        case 4:
//            button.setTitle("社会", for: .normal)
//        case 5:
//            button.setTitle("科学", for: .normal)
//        case 6:
//            button.setTitle("ビジネス", for: .normal)
        default:
            break
        }
    }
    
    // ボタンクリック時に呼び出される処理
    @objc func buttonTapped(sender:UIButton) {
        for array in btnArray {
            if array.tag != sender.tag {
                array.setTitleColor(.white, for: .normal)
                print(array.tag)
                switch array.tag {
                case 0:
                    array.setImage(whiteBtn, for: .normal)
                    array.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                    array.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                case 1:
                    array.setImage(sunBtn, for: .normal)
                    array.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                    array.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                case 2:
                    array.setImage(crossBtnImage, for: .normal)
                    array.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                    array.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                default:
                    break
                }
            }
        }
        
        if sender.tag == 0 {
            
            if let pageViewController = pageViewController {
                sender.setTitleColor(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), for: .normal)
                sender.setImage(blackBtn, for: .normal)
                sender.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                sender.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                pageViewController.setViewControllers([menuViewControllers[0]], direction: .reverse, animated: true, completion: nil) //FirstViewControllerを表示
                selected = 1
            }
            
        } else if sender.tag == 1 {
            
            // 表示切り替え（前回選択していたメニューの位置に応じてアニメーションの向きを設定）
            if (selected < 2) {
                sender.setTitleColor(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), for: .normal)
                sender.setImage(blackBtn, for: .normal)
                sender.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                sender.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                pageViewController!.setViewControllers([menuViewControllers[1]], direction: .forward, animated: false, completion: nil) //SecondViewControllerを表示
            } else {
                pageViewController!.setViewControllers([menuViewControllers[1]], direction: .reverse, animated: true, completion: nil)
            }
            selected = 2
            
        } else if sender.tag == 2 {
            
            // 表示切り替え（前回選択していたメニューの位置に応じてアニメーションの向きを設定）
            if (selected < 3) {
                sender.setTitleColor(#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), for: .normal)
                sender.setImage(blackBtn, for: .normal)
                sender.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
                sender.titleEdgeInsets = UIEdgeInsets(top: 70, left: -40, bottom: 0, right: 0)
                pageViewController!.setViewControllers([menuViewControllers[2]], direction: .forward, animated: false, completion: nil) //ThirdViewControllerを表示
            } else {
                pageViewController!.setViewControllers([menuViewControllers[2]], direction: .reverse, animated: true, completion: nil)
            }
            selected = 3
            
        }
    }
}

extension TagBtnViewController: UIScrollViewDelegate {
    

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        posY = tapBarScroll.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tapBarScroll.contentOffset.y = posY
    }
}

