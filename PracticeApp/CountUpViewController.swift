//
//  CountUpViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/10/12.
//

import UIKit

class CountUpViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    @IBAction func countUpButton(_ sender: Any) {
        count += 1
        
        textLabel.text = CountUp.countUp(num: count)

        
    }
    
    @IBAction func nextButton(_ sender: Any) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "countUp") as! CountUpTableViewController
        self.present(secondViewController, animated: true, completion: nil)
    }
    
}
