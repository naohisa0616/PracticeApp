//
//  WifiConfirmViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/09/18.
//

import UIKit
import Network

class WifiConfirmViewController: UIViewController {
    
    
    let monitor = NWPathMonitor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkWifiConnection()
    }
    
    
    func checkWifiConnection() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("通信環境あります！")
            } else {
                print("通信出来ません！")
            }
            
            print(path.isExpensive)
        }
    }
    
}
