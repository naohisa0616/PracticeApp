//
//  Practice.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/09/11.
//

import Foundation


class Practice {
    func printTexts(texts: [String]) {
        for i in texts {
            print(i)
        }
    }

    let texts = ["Hello", "World", "!"]
//    printTexts(texts: texts)
}
