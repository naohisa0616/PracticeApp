//
//  TagBtnViewController.swift
//  PracticeApp
//
//  Created by 宮崎直久 on 2021/08/27.
//

import UIKit
import Network

class TagViewController: UIViewController {

    var vc1 = UIView()
    let buttonTitleArray: [String] = ["最新","人気","フォロー","文学","社会","科学","ビジネス"]
    let monitor = NWPathMonitor()

    @IBOutlet weak var tapBarScroll: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ScrollView上に要素を配置
        horizontalScroll()
        

    }
    

    
    //横スクロール
    func horizontalScroll() {
        //vcのframe
        vc1.frame = CGRect(x: 0, y: 0, width: tapBarScroll.frame.size.width * 2, height: tapBarScroll.frame.size.height)
        //上部のスクロールビューに多数のボタンを配置
        for (row, title) in buttonTitleArray.enumerated() {
            vc1.addSubview(makeButton(tag: row, title: title))
        }
        //スクロールビューにvcを配置
        tapBarScroll.addSubview(vc1)
        //scrollViewのコンテンツサイズをvc1のサイズにする
        tapBarScroll.contentSize = vc1.bounds.size
    }

    //スクロールビューのボタンに文字を入れる
    func makeButton(tag:Int, title:String) -> UIButton {
        let button = UIButton()
        button.frame = CGRect(x: (tag*100), y: 30, width: 80, height: 55)
        button.tag = tag
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: #selector(self.onClick), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        button.layer.borderWidth = 1
        return button
    }
    
    // ボタンクリック時に呼び出される処理

    @objc func onClick(sender:UIButton){
        if sender.tag==0 {
            print("tagは0")
        }else{
            print("その他")
        }
    }

    

}


